from datetime import datetime
from urllib import request

from django.contrib.auth.models import User
from django.db import models

class Palabras(models.Model):
    pal = models.TextField()
    defi = models.TextField()
    resumen = models.TextField(default=" ")
    img = models.TextField()
    saved = models.CharField(default='n', max_length=1)
    votos = models.IntegerField(default=0)
    ncomentarios = models.IntegerField(default=0)
    fecha = models.DateTimeField(default=datetime.now, blank=True)
    usuario = models.CharField(max_length=20, default='anonimo')

class Comentario(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    palabra = models.ForeignKey(Palabras, on_delete=models.CASCADE)
    cuerpo = models.TextField()
    fecha = models.DateTimeField(default=datetime.now, blank=True)

class Voto(models.Model):
    palabra = models.ForeignKey(Palabras, on_delete=models.CASCADE)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    voto = models.CharField(default='neu', max_length=3)
    fecha = models.DateTimeField(default=datetime.now, blank=True)

class Link(models.Model):
    url = models.TextField(default ='')
    defi = models.TextField(default ='')
    img = models.TextField(default ='')
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    palabra = models.ForeignKey(Palabras, on_delete=models.CASCADE, blank=True, null=True)
    date = models.DateTimeField()

class Info(models.Model):
    web = models.TextField(default ='')
    defi = models.TextField(default ='')
    img = models.TextField(default = '')
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    palabra = models.ForeignKey(Palabras, on_delete=models.CASCADE, blank=True, null=True)
    date = models.DateTimeField()
