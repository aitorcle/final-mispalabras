from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from django.test import Client

# Create your tests here

class GetTests(TestCase):
    def test_root(self):
        """Check root resource"""
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h1 class="title">MIS PALABRAS</h1>', content)

    def test_mipagina(self):
        c = Client()
        response = c.get('/mipagina')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn(' <h5>Nombre de usuario:</h5>', content)

    def test_palabranotfound(self):
        c = Client()
        response = c.get('palabra')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h1 class="title">MIS PALABRAS</h1>', content)





