# Generated by Django 4.0.3 on 2022-05-08 17:54

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0008_palabras_votos'),
    ]

    operations = [
        migrations.AddField(
            model_name='palabras',
            name='fecha',
            field=models.DateTimeField(blank=True, default=datetime.datetime.now),
        ),
        migrations.AddField(
            model_name='palabras',
            name='ncomentarios',
            field=models.IntegerField(default=0),
        ),
    ]
