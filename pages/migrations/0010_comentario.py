# Generated by Django 4.0.3 on 2022-05-08 18:31

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('pages', '0009_palabras_fecha_palabras_ncomentarios'),
    ]

    operations = [
        migrations.CreateModel(
            name='Comentario',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cuerpo', models.TextField()),
                ('fecha', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('palabra', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pages.palabras')),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
