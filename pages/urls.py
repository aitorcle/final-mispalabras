
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('loggedin', views.loggedin, name='loggedin'),
    path('logout', views.user_logout, name='logout'),
    path('register', views.register, name="register"),
    path('mipagina',views.mipagina, name="mipagina"),
    path('info',views.info, name="info"),
    path('<pal>',views.palabra, name='palabra'),
]
