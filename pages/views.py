import random
from time import strftime

from django.contrib.auth import logout, login
from django.core.paginator import Paginator
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from .models import Palabras, Comentario, Voto, Info, Link
from .forms import UserForm, ComentarioForm, TextForm, UrlForm
import requests
from datetime import datetime, timezone
from urllib.parse import urlparse, quote
import urllib.request
from html.parser import HTMLParser
from xml.dom.minidom import parse


@csrf_exempt
def index(request):
    lista = Palabras.objects.all()
    listavotos = Palabras.objects.all().order_by("-votos")[0:5]
    ls = []
    for item in listavotos:
        if item.saved == 's':
            ls.append(item.pal)
    leng = len(lista)
    aleat = ' '
    if leng > 0:
        aleat = random.choice(ls)

    paginator = Paginator(list(reversed(lista)), 5)

    npagina = request.GET.get('page')
    obj = paginator.get_page(npagina)
    if request.method == 'GET':
        formato = request.GET.get('format')
        if formato == 'xml':
            return render(request, 'pages/palabras.xml', {'Palabras': lista}, content_type='text/xml')
        if formato == 'json':
            return render(request, 'pages/palabras.json', {'Palabras': lista}, content_type='text/json')
        else:
            response = render(request, 'pages/index.html',
                              {'Listavotos': listavotos, 'Lista': obj, 'aleat': aleat, 'len': leng, 'obj': obj})

    elif request.method == 'POST':
        articulo = request.POST['wiki']
        articulo = articulo.capitalize()
        if articulo == "":
            return (HttpResponseRedirect('/'))
        try:
            Palabras.objects.get(pal=articulo)
            response = HttpResponseRedirect("/" + articulo)
        except:
            texto = "https://es.wikipedia.org/w/api.php?action=query&format=xml&titles=" + articulo.lower() + "&prop=extracts&exintro&explaintext"
            foto = "https://es.wikipedia.org/w/api.php?action=query&titles=" + articulo.lower() + "&prop=pageimages&format=json&pithumbsize=200"
            xmltext = requests.get(texto).text
            content = xmltext.split('>')
            defi = content[9].split('<')[0]
            xmlfoto = requests.get(foto).text
            resumen = (defi[0:70] + '...')
            try:
                fcontent = xmlfoto.split('"source":')[1]
                imgurl = fcontent.split('"')[1]
                lista = Palabras(pal=articulo, defi=defi, img=imgurl, resumen=resumen)
                lista.save()
            except:
                lista = Palabras(pal=articulo, defi=defi, resumen=resumen)
                lista.save()
            response = HttpResponseRedirect("/" + articulo)
    else:
        response = 'Debes estar logeado para añadir palabras, <a href="/login">Login</a>'
    return response


@csrf_exempt
def palabra(request, pal):
    if request.method == 'GET':
        try:
            p = Palabras.objects.get(pal=pal)
            defi = p.defi
            img = p.img
            saved = p.saved
            imgfli = flickr(request, pal)
            texto = TextForm()
            imgapi = apimeme(request, pal)
            enlace = UrlForm()
            tarjeta = gest_enlace(request, pal)
            try:
                i = Info.objects.get(web='rae', palabra=p)
                drae = [i.defi, i.img]

            except Info.DoesNotExist:
                drae = rae(request, pal)
            try:
                coments = Comentario.objects.all()
                content = render(request, 'pages/palabra.html',
                                 {'pal': pal, 'defi': defi, 'img': img, 'saved': saved, 'fecha': datetime.now(),
                                  'form': ComentarioForm(), 'comentarios': coments, 'votos': p.votos, 'user': p.usuario,
                                  'drae': drae[0], 'draeimg': drae[1], 'imgfli': imgfli, 'imgapi': imgapi,
                                  'texto': texto,
                                  'enlace': enlace, 'tarjeta': tarjeta})
            except:
                content = render(request, 'pages/palabra.html',
                                 {'pal': pal, 'defi': defi, 'img': img, 'saved': saved, 'fecha': datetime.now(),
                                  'form': ComentarioForm(), 'votos': p.votos, 'drae': drae[0], 'draeimg': drae[1],
                                  'imgfli': imgfli, 'imgapi': imgapi, 'texto': texto, 'enlace': enlace,
                                  'tarjeta': tarjeta})
            i = Info.objects.all()
            li = []
            lc = []
            for item in i:
                if item.palabra == p:
                    li.append(item)
            for item in coments:
                if item.palabra == p:
                    lc.append(item)

            formato = request.GET.get('format')
            if formato == 'xml':
                return render(request, 'pages/palabra.xml', {'palabra': p, 'comentarios': lc, 'links': li},
                              content_type='text/xml')
            if formato == 'json':
                return render(request, 'pages/palabra.json', {'Palabras': p, 'comentarios': coments, 'links': li},
                              content_type='text/json')
            response = HttpResponse(content)

        except Palabras.DoesNotExist:
            lista = Palabras.objects.all()
            listavotos = Palabras.objects.all().order_by("-votos")[0:5]
            ls = []
            for item in listavotos:
                if item.saved == 's':
                    ls.append(item.pal)
            leng = len(lista)
            aleat = ' '
            if leng > 0:
                aleat = random.choice(ls)

            response = render(request, 'pages/error.html',
                              {'Listavotos': listavotos, 'Lista': reversed(lista), 'aleat': aleat, 'len': leng})
        return response

    if request.method == 'POST':
        p = Palabras.objects.get(pal=pal)
        m = Voto.objects.all()
        defi = p.defi
        img = p.img
        imgfli = flickr(request, pal)
        imgapi = apimeme(request, pal)
        texto = TextForm()
        enlace = UrlForm()
        tarjeta = gest_enlace(request, pal)
        coments = Comentario.objects.all()
        try:
            infrae = Info.objects.get(web='rae', palabra=p)
            drae = [infrae.defi, infrae.img]
        except Info.DoesNotExist:
            drae = rae(request, p.pal)
        if 'cuerpo' in request.POST:
            form = ComentarioForm(request.POST)
            if form.is_valid():
                cuerpo = form.cleaned_data['cuerpo']
                c = Comentario(usuario=request.user, cuerpo=cuerpo, palabra=p, fecha=datetime.now())
                c.save()
                coments = Comentario.objects.all()
                p.ncomentarios += 1
                p.save()

        if 'guardar' in request.POST:
            p.saved = 's'
            p.usuario = request.user.username
            p.save()

        if 'mg' in request.POST:
            print(request.POST)
            try:
                m = Voto.objects.get(usuario=request.user, palabra=p)
                if m.voto == 'neu':
                    p.votos = p.votos + 1
                    m.voto = 'pos'
                elif m.voto == 'neg':
                    p.votos = p.votos + 1
                    m.voto = 'neu'
                else:
                    p.votos = p.votos
                    m.voto = 'pos'
            except:
                m = Voto(usuario=request.user, palabra=p, voto='pos')
                p.votos = p.votos + 1
                m.voto = 'pos'
            p.save()
            m.save()

        if 'nmeg' in request.POST:
            print(request.POST)
            try:
                m = Voto.objects.get(usuario=request.user, palabra=p)
                if m.voto == 'pos':
                    p.votos = p.votos - 1
                    m.voto = 'neu'
                elif m.voto == 'neu':
                    p.votos = p.votos - 1
                    m.voto = 'neg'
                else:
                    p.votos = p.votos
                    m.voto = 'neg'
            except:
                m = Voto(usuario=request.user, palabra=p, voto='neu')
                p.votos = p.votos - 1
                m.voto = 'neu'
            p.save()
            m.save()

        content = render(request, 'pages/palabra.html', {'pal': pal, 'defi': defi, 'img': img, 'saved': p.saved,
                                                         'fecha': datetime.now(), 'form': ComentarioForm(),
                                                         'comentarios': coments, 'votos': p.votos, 'mg': m,
                                                         'user': p.usuario,
                                                         'drae': drae[0], 'draeimg': drae[1], 'imgfli': imgfli,
                                                         'imgapi': imgapi,
                                                         'texto': texto, 'enlace': enlace, 'tarjeta': tarjeta})
    return HttpResponse(content)


def loggedin(request):
    if request.user.is_authenticated:
        response = 'Logged in as ' + request.user.username
    else:
        response = 'Not login in. <a href="/login">Login</a>'
    return HttpResponse(response)


def user_logout(request):
    logout(request)
    return HttpResponse('Ha deslogeado correctamente, <a href="/">Volver a la página principal</a>')


def register(request):
    if request.method == "POST":
        form = UserForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.set_password(user.password)
            user.save()
        return HttpResponseRedirect("/")
    else:
        form = UserForm()
        context = {'form': form, }
        return render(request, 'pages/register.html', context)


def get_fecha(elemento):
    return elemento[1]


def mipagina(request):
    lista = Palabras.objects.all()
    listavotos = Palabras.objects.all().order_by("-votos")
    ls = []
    for item in listavotos:
        if item.saved == 's':
            ls.append(item.pal)
    leng = len(lista)
    aleat = ' '
    if leng > 0:
        aleat = random.choice(ls)
    if request.method == "GET":
        p = Palabras.objects.all()
        lp = []
        for item in p:
            if item.usuario == request.user.username:
                lp.append(item.pal)
        c = Comentario.objects.all()
        lc = []
        for item in c:
            if item.usuario == request.user:
                lc.append(item.cuerpo)
        v = Voto.objects.all()
        lv = []
        for item in v:
            if item.usuario == request.user:
                lv.append(item.palabra.pal)
        lpreversed = list(reversed(lp))
        lcreversed = list(reversed(lc))
        i = Link.objects.all()
        li = []
        for item in i:
            if item.usuario == request.user:
                li.append(item.url)
        lireversed = list(reversed(li))

        todo = []
        for item in p:
            todo.append((item, item.fecha))
        for item in c:
            todo.append((item, item.fecha))
        for item in i:
            todo.append((item, item.date))
        todo.sort(key=get_fecha, reverse=True)
        to = []
        if (len(lp) > 0) and (len(lc) > 0) and (len(li) > 0):
            for item in todo:
                if (type(item[0]) == type(p[0])) and (item[0].usuario == request.user.username):
                    to.append('Palabra: ' + item[0].pal + ' fecha: ' + (item[1]).strftime("%H:%M:%D"))
                if (type(item[0]) == type(c[0])) and (item[0].usuario == request.user):
                    to.append('Comentario: "' + item[0].cuerpo + '" en la palabra ' + item[0].palabra.pal + ' fecha: ' + (
                    item[1]).strftime("%H:%M:%D"))
                if (type(item[0]) == type(i[0])) and (item[0].usuario == request.user):
                    to.append('Link: ' + item[0].url + ' fecha: ' + (item[1]).strftime("%H:%M:%D"))
        paginator = Paginator(list(reversed(lista)), 5)

        npagina = request.GET.get('page')
        obj = paginator.get_page(npagina)

        formato = request.GET.get('format')
        if formato == 'xml':
            return render(request, 'pages/usuario.xml', {'Palabras': lp, 'Comentarios': lc, 'Votos': lv, 'Links': li},
                          content_type='text/xml')
        if formato == 'json':
            return render(request, 'pages/usuario.json', {'Palabras': lp}, content_type='text/json')
        content = render(request, 'pages/usuario.html', {'palabras': (lpreversed), 'comentarios': (lcreversed),
                                                         'votos': (lv), 'links': lireversed, 'todo': to,
                                                         'Listavotos': listavotos[0:5], 'Lista': obj,
                                                         'aleat': aleat, 'len': leng, 'obj':obj})
        return HttpResponse(content)


class ParserDefi(HTMLParser):
    def __init__(self):
        super().__init__()
        self.defi = ''
        self.atr = []
        self.select = False
        self.val = ''
        self.sel = False

    def handle_starttag(self, tag, attrs):
        if tag == 'meta':
            self.atr.append(('Final', 'Inicio'))
            for atr in attrs:
                self.atr.append(atr)
            for at in self.atr:
                if at[0] == 'Final' and at[1] == 'Inicio':
                    self.val = ''
                    self.select = False
                    self.sel = False
                if self.select:
                    if at[0] == 'content':
                        self.defi = at[1]
                        self.select = False
                        break
                if at[0] == 'property' and at[1] == 'og:description':
                    self.select = True
                    if self.sel:
                        self.defi = self.val
                        break
                if at[0] == 'content':
                    self.sel = True
                    self.val = at[1]


class ParserImg(HTMLParser):
    def __init__(self):
        super().__init__()
        self.img = ''
        self.atr = []
        self.sel_img = False
        self.val = ''
        self.sel = False

    def handle_starttag(self, tag, attrs):
        if tag == 'meta':
            self.atr.append(('Final', 'Inicio'))
            for atr in attrs:
                self.atr.append(atr)
            for at in self.atr:
                if at[0] == 'Final' and at[1] == 'Inicio':
                    self.val = ''
                    self.sel_img = False
                    self.sel = False
                if self.sel_img:
                    if at[0] == 'content':
                        self.img = at[1]
                        self.sel_img = False
                        break
                if at[0] == 'property' and at[1] == 'og:image':
                    self.sel_img = True
                    if self.sel:
                        self.img = self.val
                        break
                if at[0] == 'content':
                    self.sel = True
                    self.val = at[1]


def rae(request, pal):
    parserdefi = ParserDefi()
    parserimg = ParserImg()
    if request.method == 'POST' and 'rae' in request.POST:
        url = 'https://dle.rae.es/' + str(quote(pal))
        user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
        headers = {'User-Agent': user_agent}
        req = urllib.request.Request(url, headers=headers)

        with urllib.request.urlopen(req) as response:
            html = response.read().decode('utf8')
            parserdefi.feed(html)
            parserimg.feed(html)
            definicion = [parserdefi.defi, parserimg.img]
        r = Info(web='rae')
        r.defi = definicion[0]
        r.palabra = Palabras.objects.get(pal=pal)
        r.img = definicion[1]
        r.usuario = request.user
        r.date = datetime.now()
        r.save()
    else:
        definicion = [None, None]
    return definicion


def flickr(request, pal):
    if request.method == 'POST' and 'flickr' in request.POST:
        try:
            url = "https://www.flickr.com/services/feeds/photos_public.gne?tags=" + str(quote(pal))
            xml = parse(urllib.request.urlopen(url))
            feed = xml.getElementsByTagName('feed')
            entry = feed[0].getElementsByTagName('entry')
            link = entry[0].getElementsByTagName('link')
            for tag in link:
                atributo = tag.getAttribute('rel')
                if atributo == 'enclosure':
                    href = tag.getAttribute('href')

        except AttributeError:
            href = ''
        except IndexError:
            href = ''
        i = Info(web='flickr')
        i.img = href
        i.usuario = request.user
        i.palabra = Palabras.objects.get(pal=pal)
        i.date = datetime.now()
        i.save()
    else:
        try:
            p = Palabras.objects.get(pal=pal)
            href = Info.objects.get(web='flickr', palabra=p).img
        except Info.DoesNotExist:
            href = None
    return href


def apimeme(request, pal):
    if request.method == 'POST' and 'apimeme' in request.POST:
        form = TextForm(request.POST)
        if form.is_valid():
            texto = form.cleaned_data['text']
            option = form.cleaned_data['option']
            if option == '1':
                img = "http://apimeme.com/meme?meme=10-Guy&top=" + str(quote(texto)) + "&bottom=" + str(quote(pal))
            elif option == '2':
                img = "http://apimeme.com/meme?meme=Ancient-Aliens&top=" + str(quote(texto)) + "&bottom=" + str(
                    quote(pal))
            elif option == '3':
                img = "http://apimeme.com/meme?meme=Advice-Yoda&top=" + str(quote(texto)) + "&bottom=" + str(quote(pal))

        i = Info(web='apimeme')
        i.img = img
        i.usuario = request.user
        i.palabra = Palabras.objects.get(pal=pal)
        i.date = datetime.now()
        i.save()
    else:
        try:
            p = Palabras.objects.get(pal=pal)
            img = Info.objects.get(web='apimeme', palabra=p).img
        except Info.DoesNotExist:
            img = None
    return img


def info(request):
    lista = Palabras.objects.all()
    listavotos = Palabras.objects.all().order_by("-votos")[0:5]
    ls = []
    for item in listavotos:
        if item.saved == 's':
            ls.append(item.pal)
    leng = len(ls)
    aleat = ' '
    if leng > 0:
        aleat = random.choice(ls)
    paginator = Paginator(list(reversed(lista)), 5)

    npagina = request.GET.get('page')
    obj = paginator.get_page(npagina)

    response = render(request, 'pages/info.html',
                      {'Listavotos': listavotos, 'Lista': obj, 'aleat': aleat, 'len': leng,'obj':obj})
    return HttpResponse(response)


def gest_enlace(request, pal):
    parserdefi = ParserDefi()
    parserimg = ParserImg()
    if request.method == 'POST' and 'btnenlace' in request.POST:
        try:
            form = request.POST['enlace']
            response = urllib.request.urlopen(form)
            html = response.read().decode('utf8')
            parserdefi.feed(html)
            parserimg.feed(html)
            definicion = [parserdefi.defi, parserimg.img]
            r = Link(url=form)
            r.defi = definicion[0]
            r.palabra = Palabras.objects.get(pal=pal)
            r.img = definicion[1]
            r.usuario = request.user
            r.date = datetime.now()
            r.save()
        except UnicodeError:
            r = Link(url=form)
            r.defi = 'Not found'
            r.palabra = Palabras.objects.get(pal=pal)
            r.img = 'Not found'
            r.usuario = request.user
            r.date = datetime.now()
            r.save()
    try:
        p = Palabras.objects.get(pal=pal)
        info = Link.objects.filter(palabra=p)
    except Link.DoesNotExist:
        info = None
    return info


def xmlpage(request):
    if request.method == 'GET':
        p = Palabras.objects.all()
        formato = request.GET.get('format')
        if formato == 'xml':
            return render(request, 'pages/palabras.xml', {'Palabras': p}, content_type='text/xml')
