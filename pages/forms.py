from django import forms

from django.contrib.auth.models import User
from .models import Comentario

class UserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('username', 'email', 'password',)
        widgets = {
           'password': forms.PasswordInput()

        }

class ComentarioForm(forms.ModelForm):

    class Meta:
        model = Comentario
        fields = ('cuerpo',)

class TextForm(forms.Form):
    choices = [(1,'10-Guy'),(2,'alien'),(3,'yoda')]
    option = forms.ChoiceField(label=False,choices=choices)
    text = forms.CharField(label=False, widget=forms.Textarea(attrs={'rows':'1'}), max_length=20)

class UrlForm(forms.Form):
    enlace = forms.URLField(label=False, widget=forms.URLInput(attrs={'rows':'1'}), max_length=256)